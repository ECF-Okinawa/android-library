# android-library
### summary
AndroidのBLE（Bluetooth Low Device）を軽くラッピングしたライブラリ
結局元のライブラリと変わったのか？  
Android Library形式（.aar）で作成しているため、Android Projectで使う想定

### usage
build.gradle(app)に下記を追加
```
repositories{
    maven{
        url 'https://bitbucket.org/ECF-Okinawa/android-library/raw/master/repository'
    }
}
dependencies {
    ・・・
    implementation 'com.e3factory:blelibrary:1.1.4'
    ・・・
}
```

で行けるはず。

### 接続まで
